import { BACKEND_HOST } from "../constants";

export function getQueryParam(paramName) {
  const queryString =
    location.hash !== "" ? location.hash.substr(1) : location.search.substr(1);
  const queryStringParams = queryString.split("&");
  const queryStringKeyValues = queryStringParams.map((x) => x.split("="));
  const codeParam = queryStringKeyValues.filter((x) => x[0] === paramName);
  if (codeParam.length === 0) {
    return null;
  }
  return codeParam[0][1];
}

export function getURLForImage(imagePath) {
  if (imagePath !== null && imagePath !== undefined) {
    if (imagePath.charAt(0) === '/') {
      if (imagePath.charAt(1) === '/') imagePath = imagePath.slice(17, imagePath.length);
      if (imagePath.charAt(1) === 'a') imagePath = imagePath.slice(16, imagePath.length);
    }
    return `${BACKEND_HOST}/storage/${imagePath}`
  }
  return "https://placekitten.com/500/500";
}
