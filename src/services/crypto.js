import request from "./api";

export async function getAllExchanges() {
  const response = await request.get("/exchanges/");
  return response.data;
}

export async function getAllCurrencies() {
  const response = await request.get("/crypto/");
  return response.data;
}

export async function getExchange(id) {
  const response = await request.get(`/exchanges/${id}`);
  return response.data;
}

export async function getCurrency(id) {
  const response = await request.get(`/crypto/${id}`);
  return response.data;
}

export async function getPrices(currency = "all", exchange = "all") {
  const now = new Date();
  const yesterday = new Date();
  yesterday.setDate(now.getDate() - 1);
  const url = `/crypto_api/price_history?exchange=${exchange}&currency=${currency}&start_time=${yesterday.toISOString().slice(0, -1)}&end_time=${now.toISOString().slice(0, -1)}`
  const response = await request.get(url);
  return response.data;
}

export async function userSubscribedToEx(ex_id) {
  const res = await request.get(`/users/me/subscribed_to_exchange/${ex_id}`);
  return res.data;
}

export async function userSubscribedToCurr(curr_id) {
  const res = await request.get(`/users/me/subscribed_to_currency/${curr_id}`);
  return res.data;
}

export async function subscribeToEx(user_id, ex_id) {
  const res = await request.post(`/exchanges/${ex_id}/subscribe`, { user_id });
  return res.data;
}

export async function subscribeToCurr(user_id, curr_id) {
  const res = await request.post(`/crypto/${curr_id}/subscribe`, { user_id });
  return res.data;
}
